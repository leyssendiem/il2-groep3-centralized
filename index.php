<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Swaggadon</title>
	<link rel="stylesheet" href="normalize.css">
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<h1>Centralized</h1>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident ipsam quis eos magni aliquam error, doloremque sapiente expedita dolore saepe eius, iste ea necessitatibus id deserunt maiores, tempora repellendus rerum.</p>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem dolore, dolorem delectus veniam voluptas praesentium in culpa consequatur hic architecto nulla a, odio fugiat dolor doloremque eius quidem. Ad nostrum corrupti tenetur aliquid aut commodi, mollitia iusto ipsum aliquam, non ducimus magni eum molestias blanditiis quod nam optio ut impedit maxime cupiditate, nesciunt necessitatibus natus quibusdam repellat. Quisquam, nesciunt neque eius! Facilis molestias corrupti ea cupiditate consequatur laudantium, ducimus quas repellat nobis quasi quia, itaque aut amet illo ullam voluptatibus doloremque fuga provident necessitatibus sequi dicta, id quisquam. Sit impedit id nobis nemo porro, sequi ad sed quo nostrum nesciunt, magni omnis facilis. Harum vero ullam rerum, ut, doloremque sequi animi aliquam perspiciatis voluptatem excepturi, aperiam reiciendis facilis quos fuga iusto sint ducimus illum et. Facilis fuga, voluptatem accusantium quae dolor deleniti cum labore commodi fugit asperiores alias quia, consectetur nisi illum placeat. Officiis sint provident eos architecto accusantium? Beatae.</p>
	<footer>
		copyright
	</footer>
	<script src="jquery.js"></script>
</body>
</html>